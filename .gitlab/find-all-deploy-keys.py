#!/usr/bin/env python3
#
# find all the Deploy Keys in all the @fdroid projects

import gitlab
import os
from colorama import Fore, Style

private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
if not private_token:
    print(
        Fore.RED
        + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
        + Style.RESET_ALL
    )
    exit(1)

forbidden_keys = [
    'v02HBt9YkphUoYgwMZOwtihLnxvDdBCymPew9SlA7d4',  # @fdroidci checkupdates
]
gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)
groups = [gl.groups.get('fdroid', lazy=True)]
for subgroup in groups[0].subgroups.list():
    groups.append(gl.groups.get(subgroup.id, lazy=True))
for group in groups:
    print(dir(group))
    print(f'@{group.id}')
    for project_entry in group.projects.list(all=True):
        print('----------------------------------------------------')
        print(f'@{group.id}/{project_entry.path}')
        if project_entry.path in ('internal-twif-preview', ):
            # this job returns 403 Forbidden for some unknown reason
            print('Skipping', project_entry.path)
            continue
        project = gl.projects.get(project_entry.get_id(), lazy=True)
        for key in project.keys.list(iterator=True):
            print(key)
            if key.fingerprint_sha256 in forbidden_keys:
                sys.exit(1)
