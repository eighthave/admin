#!/usr/bin/env python3
#

import gitlab
import os
from colorama import Fore, Style

private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
if not private_token:
    print(
        Fore.RED
        + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
        + Style.RESET_ALL
    )
    exit(1)

gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)
ci_project_path = os.getenv('CI_PROJECT_PATH')
if not ci_project_path:
    print(
        Fore.RED
        + 'ERROR: CI_PROJECT_PATH not found!'
        + Style.RESET_ALL
    )
    exit(1)

project = gl.projects.get(ci_project_path, lazy=True)
tmpdir = os.path.join(os.getenv('HOME'), 'Downloads', os.path.basename(__file__)[:-3])
if not os.path.exists(tmpdir):
    os.mkdir(tmpdir)
print(f'Working in temporary directory {tmpdir}')
os.chdir(tmpdir)
for job in project.jobs.list(iterator=True):
    print(f'https://gitlab.com/fdroid/fdroiddata/-/jobs/{job.id}', flush=True)
    logfile = f"{ci_project_path.replace('/', '-')}-{job.id}-log.txt"
    if not os.path.exists(logfile):
        with open(logfile, "wb") as fp:
            job.trace(streamed=True, action=fp.write)
print(f'The job logs are in {tmpdir}')
