#!/usr/bin/python3

import gitlab
import pygit2
import os
import re
import sys
import ruamel.yaml


def main():
    if len(sys.argv) > 2:
        print(f"Usage: {sys.argv[0]} [username]")
        sys.exit(1)
    if len(sys.argv) == 2:
        username = sys.argv[1]
    else:
        username = "checkupdates-bot"

    repo = pygit2.Repository(".")
    gl = gitlab.Gitlab("https://gitlab.com")
    users = gl.users.list(username=username)
    if len(users) == 0:
        print(f"ERROR: No users found for {username}")
        sys.exit(1)
    if len(users) > 1:
        print(f"ERROR: found multiple users for {username}:")
        for u in users:
            print(u)
        sys.exit(1)
    user = users[0]

    remotes = [name for name in repo.remotes.names()]
    if "upstream" in remotes:
        url = repo.remotes["upstream"].url
    elif "origin" in remotes:
        url = repo.remotes["origin"].url
    else:
        print('\nERROR: No "upstream" or "origin" remote found!')
        sys.exit(1)
    if url.endswith(".git"):
        url = url[:-4]
    print(f"Searching for activity by {user.username} in {url}")

    notable_path_changes = []
    notable_diffs = []

    for event in user.events.list(iterator=True):
        if hasattr(event, "push_data"):
            project = gl.projects.get(event.project_id)
            if hasattr(project, "forked_from_project"):
                project_url = project.forked_from_project.get("web_url")
                direct = False
            else:
                project_url = project.web_url
                direct = True
            if url != project_url:
                print(event.created_at, project_url, "not in local git repo")
                continue
            print(event.created_at, project_url, "(pushed)" if direct else "(merged)")

            commit_url = f'{project_url}/-/commit/{event.push_data["commit_to"]}'
            diff = repo.diff(
                event.push_data["commit_from"], event.push_data["commit_to"]
            )
            # print(dir(diff))
            # print(diff.stats)
            paths = set()
            for delta in diff.deltas:
                paths.add(delta.old_file.path)
                paths.add(delta.new_file.path)
            # print(sorted(paths))
            for path in paths:
                m = re.match(r"metadata/(\S+)\.yml", path)
                if not m:
                    print("path match", path, sep="\t")
                    notable_path_changes.append(
                        [event.created_at, commit_url, direct, paths]
                    )

            matches = False
            m = re.search("-Repo:.*", diff.patch)
            if m:
                matches = True
            m = re.search("[-+] *sudo:.*", diff.patch)
            if m:
                matches = True
            if matches:
                print("diff match", commit_url, sep="\t")
                notable_diffs.append([event.created_at, commit_url, direct, diff.patch])

    yaml = ruamel.yaml.YAML()
    yaml.default_flow_style = False
    with open(
        os.path.join("artifacts", os.path.basename(__file__) + ".yaml"), "w"
    ) as fp:
        yaml.dump({"path_changes": notable_path_changes, "diffs": notable_diffs}, fp)


if __name__ == "__main__":
    main()
