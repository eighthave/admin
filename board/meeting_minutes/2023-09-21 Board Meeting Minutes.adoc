= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-09-17 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, John Sullivan, Michael Downey, Andrew Lewman, Hans, Max Mehl

=== Guests
SylvieLorxu, Licaon_Kter, Fay, Izzy, uniq

* Meeting Locationfootnote:[https://meet.calyx.net/OverallStrokesDeriveAutomatically]
* Previous meeting's notesfootnote:[https://pad.riseup.net/p/fhapnwz88Prwa1EM5z7]

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conductfootnote:[https://f-droid.org/en/docs/Code_of_Conduct/]. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda
                          
. CoC/Community Council Call status
.. Review community feedback
.. Formal vote to charter community council
.. Set up meeting with Gabriel from FSFE CARE team to support the F-Droid CoC team.
. Follow up on legal request(s) per mailing list
.. SFLC India Letter of Engagement
.. Discuss how to engage w/Commons Conservancy & agreement processes
.. UK takedown
.. Hetzner takedown notice: https://gitlab.com/fdroid/admin/-/issues/428
.. US States' Attornies General lawsuit
. New Board Member election process
.. 'board onboarding' document: https://pad.riseup.net/p/apbK4erae-oJyAYW1SK1-keep
. Set up subcommittees
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
. Updates on budget (MD)
.. https://monitor.f-droid.org/donations
. Approve minutes from previous meeting
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
. Community Engagement (Community members can propose agenda items below)
.. F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions.
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
... Long-term: We should set up a forum where community members can propose agenda items and contribute asynchronously.
.. Clarify scope of CoC and Community Council mandate https://gitlab.com/fdroid/fdroid-website/-/merge_requests/962#note_1562507061
.. Clarify procedures core contributors must follow to gain access to various accounts and internal teams (e.g. outreach and the official mastodon account) and document current access and members. Also to avoid inconsistent application as it would be unfortunate if some team members are required to jump through more hoops than others for the same access.

== Votes

. Michael motions to approve to charter the community council and to come back to us with their scope
.. Max and Andrew Second.
.. No Discussion
.. Approved.

. Morgan mostions to approve up to US$5,000 for CoC training
.. Andrew Seconds.
.. No Discussion.
.. Approved.

. Morgan motions to approve August meeting minutes
.. Andrew Seconds.
.. No Discussion.
.. Approved.

. Morgan motions to approve July meeting minutes
.. Michael Seconds.
.. No discussion.
.. Approved.

== Notes on Agenda: 
. CoC/Community Council Call status
.. Review community feedback
.. Formal vote to charter community council
... One concern about the existence of a community council at all, but no concerns about any of the candidates themselves
.. Clarify scope of CoC and Community Council mandate https://gitlab.com/fdroid/fdroid-website/-/merge_requests/962#note_1562507061
... Does CoC cover apps, images, etc? Does the Community Council cover these things?
.... Qualified yes, with the expectation that the council will propose scope for board review (timeline: get a draft proposal to board by 1 week prior to next board meeting)
... MD motion to charter council: (seconds from Andrew & Max) Andrew, Max, John, Michael vote yes (Morgan abstains as the board liason to the Community Council), HC voted yes. Motion passes.
... Set up mailing list for Community Council group (Sylvia)
... How/when will Community Council meet
... Expense for training Community council 
.... Morgan propose motion to approve up to #3,000 for training (Andrew Seconds) Morgan, Michael, Andrew, Max, John, Hans approve. Motion passes.
.. Set up meeting with Gabriel from FSFE CARE team to support the F-Droid CoC team.
. Follow up on legal request(s) per mailing list
.. SFLC India Letter of Engagement
.. Discuss how to engage w/Commons Conservancy & agreement processes
... We have yet to hear back from them via email or text, so we're still at an impass
... Ask them how best to log issues and track things that we need from them from time to time
... Figure out how/when we need to consider an alternate plan if they just won't serve as the institutional representative
.. UK takedown
... Still getting passed around to a couple of lawyers but will follow up with John this weekend. No follow ups from the people
.. Hetzner takedown notice: https://gitlab.com/fdroid/admin/-/issues/428
... Targeted to the German server under German law. If we move our servers somewhere with more lax legal requirements for such things (i.e. Dutch) it might minimize these issues (Hans) * but is vastly more expensive
.... Look into contact(s) at Greenhost (Michael working with Hans & Uniq)
... Look at some sort of CDN in case server needs to be moved to another jurisdiction (Michael)
... Already set up on Cloudflare to sponsor us, but it brings up privacy concerns of our own b/c generally CDNs are not based on free software and we can't know everything that is going on
... If this went to court, we would probably win with a decent lawyer
.. What process should the team follow for a takedown notice?
... We don't have one, Morgan will start an email thread to take this off call to start forming a policy
. New Board Member election process
.. 'board onboarding' document: https://pad.riseup.net/p/apbK4erae-oJyAYW1SK1-keep
. Set up subcommittees
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Finance/Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
... Volunteers: Michael (as Treasurer), 
.. Governance (Elections, policy, compliance)
... [Moved from Community Engagement agenda] Clarify procedures core contributors must follow to gain access to various accounts and internal teams (e.g. outreach and the official mastodon account) and document current access and members. Also to avoid inconsistent application as it would be unfortunate if some team members are required to jump through more hoops than others for the same access.
... Process for 
.. Morgan will send out an email to asynchronously request board members volunteer for committees they are willing and able to serve on
.. John will update the board onboarding doc, after pinging the mail thread for any more contributions
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over) Please give Sylvia the emails that should be used (same as board emial for Morgan)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
.. Make this the primary Kieran task
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
.. Stalled because we cannot get ahold of anybody at commons conservancy, so we're stalled
.. Should we send a postcard? 
.. Should we ask other projects under commons conservancy how they communicate? (Michael)
. Updates on budget (MD)
.. https://monitor.f-droid.org/donations
.. Current funds: US $ 101,904.95
.. NL net grant 50,000 euros for working on the build server approved! (direct gift to developers or some amount to F-Droid)
.. electric horizons application is progressing (200,000 euros) (Fiscal Sponsor, set up through NL net)
.. OTF rapid response fund (had a call, submitted a contract ($50,000 to make website work when it's blocked) (contract through Austrian company)
... Potentially willing to fund a large project for making our core infrastructure more robust
. Approve minutes from previous meeting
.. Morgan: call to approve August Meeting Minutes (Andrew Second) Michael, Morgan, John, Hans, Andrew approve, Max abstain. Motion passes.
.. Morgan: call to approve July Meeting Minutes (Michael Second) Micahel, John, Andrew, Morgan, approve, Max and Hans Abstain. Motion passes.
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
. Community Engagement (Community members can propose agenda items below)
.. F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions.
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
... Long-term: We should set up a forum where community members can propose agenda items and contribute asynchronously.
.. Clarify scope of CoC and Community Council mandate https://gitlab.com/fdroid/fdroid-website/-/merge_requests/962#note_1562507061
.. Clarify procedures core contributors must follow to gain access to various accounts and internal teams (e.g. outreach and the official mastodon account) and document current access and members. Also to avoid inconsistent application as it would be unfortunate if some team members are required to jump through more hoops than others for the same access.