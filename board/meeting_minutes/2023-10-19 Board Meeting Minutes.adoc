= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-10-19 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, Michael Downey, Andrew Lewman, Hans-Christoph Steiner, Matthias Kirschner

=== Guests
Fay, Izzy

* Meeting Locationfootnote:[https://meet.calyx.net/OverallStrokesDeriveAutomatically]
* Previous meeting's notesfootnote:[https://pad.riseup.net/p/fhapnwz88Prwa1EM5z7]

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conductfootnote:[https://f-droid.org/en/docs/Code_of_Conduct/]. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda                         
. CoC/Community Council Update
.. Due to scheduling conflicts we have been delayed
.. Set up meeting with Gabriel from FSFE CARE team to support the F-Droid CoC team.
. Follow up on legal request(s) per mailing list
.. SFLC India Letter of Engagement
.. Discuss how to engage w/Commons Conservancy & agreement processes
.. UK takedown
.. Hetzner takedown notice: https://gitlab.com/fdroid/admin/-/issues/428
.. US States' Attornies General lawsuit
. New Board Member election process
.. 'board onboarding' document: https://pad.riseup.net/p/apbK4erae-oJyAYW1SK1-keep
. Set up subcommittees
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
.. Governance subcommittee
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
. Updates on budget (MD)
.. https://monitor.f-droid.org/donations
.. Our balance is $104,069.35. Donations were down slightly in September but not outside of the norm.
. Approve minutes from previous meeting
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
. Community Engagement (Community members can propose agenda items below)
.. Has this been resolved? F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions.
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
... Long-term: We should set up a forum where community members can propose agenda items and contribute asynchronously.
.. Clarify scope of CoC and Community Council mandate https://gitlab.com/fdroid/fdroid-website/-/merge_requests/962#note_1562507061
.. Clarify procedures core contributors must follow to gain access to various accounts and internal teams (e.g. outreach and the official mastodon account) and document current access and members. Also to avoid inconsistent application as it would be unfortunate if some team members are required to jump through more hoops than others for the same access.
.. https://gitlab.com/fdroid/admin/-/issues/401 needs to be extended again.
.. Who should get the board meeting announcement email? Just Sylvia and the board?
... For now, include Izzy and Fay, TBD best method long term for announcements


== Votes

. Morgan motions to extend status of the Community Council (Izzy, Fay, Sylvia)
.. Andrew seconds.
.. Discussion: extend until when? Until the community council can meet and get complete training. November/December 2023 at least.
.. Passed/Approved.

. Morgan motions to approve minutes from previous meeting (2023-09-21)
.. Andrew seconds.
.. Discussion: We will include votes that happen in the meetings in the minutes (removing any sensitive information that should not be public)
... Fay: Anything in the pad is already public, so if we want to keep something more private then keep that in mind
.. Approved. Matthias abstains because he wasn't there.

. 

== Notes on Agenda: 
. CoC/Community Council Update
.. Due to scheduling conflicts we have been delayed
.. Set up training schedules (there will need to be time to schedule ~at least a week, check available dates)
.. Set up meeting with Gabriel from FSFE CARE team to support the F-Droid CoC team.
... Gabriel Ku Wei Bin <gabriel.ku@fsfe.org> 
... Sage might have a public repo as well
.. Morgan proposes: Vote to continue/upgrade Izzy/All members of the ComCo with moderator status. Andrew Seconds. Michael, Matthias, Hans, Morgan, Michael, Andrew +1
. Follow up on legal request(s) per mailing list
.. Still waiting for communication with Commons Conservancy. Hans will email Sherbin (sp?). Otherwise Matthias can ping Michiel
... If we can't get a response by next month's board meeting, we will discuss alternate options
.. SFLC India Letter of Engagement
.. Discuss how to engage w/Commons Conservancy & agreement processes
.. UK takedown
.. Hetzner takedown notice: https://gitlab.com/fdroid/admin/-/issues/428
.. US States' Attornies General lawsuit (Hans was asked to be a witness, but we can't hire a lawyer so hasn't been involved. The case might be wrapping up)
. New Board Member election process
.. 'board onboarding' document: https://pad.riseup.net/p/apbK4erae-oJyAYW1SK1-keep
.. Start thinking about criteria for the voting pool (whose voices need to be taken into account?
.. Ask for solicitation of members (as we did before)
. Set up subcommittees
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
.. Governance subcommittee
. Transferring domain names to board control.
.. Still having trouble contacting Kieran, we will keep trying
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
. Updates on budget (MD)
.. https://monitor.f-droid.org/donations
.. Our balance is $104,069.35. Donations were down slightly in September but not outside of the norm.
.. Once we start spending money (on things like training) Michael will establish a spreadsheet to keep track.
.. Ideally, a short monthly report eventually, but we'll get to that once the committee forms
.. $50k grant from OTF signed, NL net grant application submitted for 50k euro grant
. Approve minutes from previous meeting
.. Morgan proposes vote, Andrew seconds, Michael, Morgan, Hans, Andrew approve (Matthias abstains because he wasn't there)
.. We will include votes that happen in the meetings in the minutes (removing any sensitive information that should not be public)
... Fay: Anything in the pad is already public, so if we want to keep something more private then keep that in mind
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
.. Start with a survey of how many people are on each
... Ask what people's perferred forms of communication are (primary and secondary options)
.... may be easiest for everyone to just send an email thread to board@ with their current tools? Action: MD will start this process with a "due date" of Nov. 14 but sooner better. 
... Fay has an idea of this ecosystem already, the main issue is that people have specific feelings about what channels they are willing to use. We might just need the board to make a decision. 
... Matrix can send email notifications per room (if that helps some people).
... Is the board the right authority to make the decision community-wide?
... Bridging has its own problems -- and it hasn't happened consistently to-date
. Community Engagement (Community members can propose agenda items below)
.. Has this been resolved? F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions.
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
... Long-term: We should set up a forum where community members can propose agenda items and contribute asynchronously.
.. Clarify scope of CoC and Community Council mandate https://gitlab.com/fdroid/fdroid-website/-/merge_requests/962#note_1562507061
.. Clarify procedures core contributors must follow to gain access to various accounts and internal teams (e.g. outreach and the official mastodon account) and document current access and members. Also to avoid inconsistent application as it would be unfortunate if some team members are required to jump through more hoops than others for the same access.
.. https://gitlab.com/fdroid/admin/-/issues/401 needs to be extended again.
.. Who should get the board meeting announcement email? Just Sylvia and the board?
... For now, include Izzy and Fay, TBD best method long term for announcements