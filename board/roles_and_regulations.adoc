= F-Droid Proposed Board Roles & Regulations
Rivka Karasik; Andrew Lewman; Michael Pöhn; Jochen Sprickerhof; Morgan Lemmer Webber; Licaon Kter; Max Mehl; Michael Downey; Matthias Kirschner; John Sullivan
:sectanchors:
:sectlinks:
:url-repo: https://gitlab.com/fdroid/admin

This document is copyright: The Commons Conservancy and F-Droid. It can be used under a Creative Commons Attribution 4.0 International license.

The F-Droid Board MAY assign named roles to qualified individuals associated to the F-Droid Programme, to provide advice and assistance on specific issues and task to the Board. The associated requirements and responsibilities are formalised in this document, published as a Regulation of F-Droid.

F-Droid only has non-remunerate positions, including its Board of Directors. Any volunteer MAY resign at any time from their role within F-Droid by giving written notice, or notice by email, of their resignation to the F-Droid Chair or through a email channel established by F-Droid for this purpose, and such resignation shall take effect at the time specified therein, or, if not specified, at the time of its receipt.

== Directors

As per its Statutes, F-Droid is governed by the F-Droid Board of Directors. Each Directors has the following tasks:

* to constructively challenge and contribute to the development of strategy within the Programme.
* to scrutinise the performance of the Programme in meeting agreed goals and objectives
* where necessary, remove friction and conflicts between developers
* to validate that technical and other information relevant to decision within the Programme is accurate and that controls and systems of risk management are robust and defensible.
* to assess the performance of the Board itself

Directors do not receive any salaries for their services, nor can they claim travel expenses or subsistence costs to attend any in-person meetings. In exceptional circumstances the F-Droid Board MAY assist in seeking external sponsorship to enable a certain Director to participate in activities within the Programme.

Any vacancy on the F-Droid Board caused by death, resignation, removal or other cause MAY be filled by a simple majority vote of the remaining Directors (even if less than a quorum). A Director so elected is entitled to hold office until the next F-Droid Board Elections, at which time a regular successor SHALL be duly elected and qualified.

== Defined Roles

The Board of Trustees shall elect from among the Trustees the following officers: a Chair, at least one and no more than two Vice Chairs, and Board committee chairs.

. Chair.
The Chair shall, when present, preside at all meetings of the Board of Trustees. The Chair shall have general supervision of the affairs of F-Droid and shall make reports to the Board of Trustees at meetings and other times as necessary to keep Trustees informed of F-Droid activities.
. Vice Chairs.
The Vice Chairs shall perform the duties and have the powers of the Chair when the Chair is absent or unable to perform their duties. If there are two Vice Chairs, the Chair shall specify which Vice Chair takes priority when the Chair is absent. Other duties of the Vice Chairs may be designated by the Board of Trustees or the Chair.
. Clerk. The Clerk shall keep records of all meetings of the Board and make a report thereon and shall issue calls and notices of meetings of the Board.
. Treasurer. The Treasurer shall, subject to the direction of the Board, be responsible for expressing the desire and intention of the Board in how to spend or direct funds held by community members on behalf of the Programme. In this role, the Treasurer serves as the financial liason to the community.
. Board Committee Chairs.
Board committee chairs shall perform the duties and have the powers set forth in the charters of the committees of which they are the chair and as determined by the Board from time to time.
. Committees and Taskforces.
In order to efficiently fulfill its tasks, the Board may establish specialized committees and taskforces, as well as assign named roles to qualified individuals to provide advice and assistance on specific issues. The associated qualifications, tasks and responsibilities SHALL be formalised by publication as part of the Regulations of F-Droid.

== Technical Lead

From among the Core Developers, the F-Droid Board MAY appoint (or remove) a Technical Lead by simple majority vote. A Technical Lead serves a term of 24 months or until they resign or are removed, and can stand for re-election. The Technical Lead is an ex officio voting member of the F-Droid Board.

In the event of a vacancy of this role, its responsibilies fall generally to the Board of Directors.

The duties of the Technical Lead are:

* Overseeing the code review process
* Scheduling votes on appointment of new Core Developers
* Organising training for new Core Developers
* Organising succession planning for Core Developers and the Technical Lead
* Representing the Core Developers in the F-Droid Board

The Technical Lead has the ability, in exceptional circumstances, to overrule a technical decision by other Core Developers; such a decision MAY in turn be appealed with the F-Droid Board. During the decision process on such an appeal, the Technical Lead refrains from voting.

== Core Developers

Core Developers are reliable contributors to F-Droid that have earned the priviledge and responsibility to review and commit source code contributions from other developers into the software repositories of F-Droid, after scrutinising if that code adheres to the conditions set by the Programme.

Core developers act as the technical gatekeepers to keep the F-Droid software safe and maintainable, and provide the sustained effort that keeps the project going over time and do most of the day-to-day work in F-Droid.

Core Developers MUST strive to keep the interests of the Programme separate of possible organisational affiliations, and SHALL make it clear when a potential conflict of interest threatens to emerge. In case a Core Developer violates this principle, the F-Droid Board MAY revoke their status as Core Developer or revoke their commit access, either temporarily or permanently at the discretion of the F-Droid Board.

The Technical Lead is responsible for publishing the list of current Core Developers on the website of F-Droid. Core Developers MAY be listed pseudonymously.

== Other named roles

The F-Droid Board MAY create and abandon additional permanent and temporary positions, and appoint individuals to such positions through a simple absolute majority vote.

An example would be a Vice Chair, Secretary or liaison to some other organisation. The F-Droid Board MAY delegate the right to handle certain decisions to an individual officer, committee or taskforce, this does not reduce the responsibility of the F-Droid Board.

The individuals fulfilling the roles defined in this Regulation act as fiduciaries with regard to the F-Droid Programme, and their duties include, but are not limited to, the fiduciary duty of care and the fiduciary duty of loyalty.

Agreed on (2023-01-19), (virtual) by:

    Michael Downey

    Matthias Kirschner

    Andrew Lewman

    Morgan Lemmer-Webber

    John Sullivan

    Max Mehl


